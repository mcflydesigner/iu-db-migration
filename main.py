import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import psycopg2
from math import ceil
from datetime import datetime
import calendar
import base64
from psycopg2.extensions import AsIs

"""
    Block of constants.
"""
ORIGINAL_TABLES_WITH_PK = {
    ('staff', ('staff_id',)),
    ('actor', ('actor_id',)),
    ('address', ('address_id',)),
    ('category', ('category_id',)),
    ('city', ('city_id',)),
    ('country', ('country_id',)),
    ('customer', ('customer_id',)),
    ('film', ('film_id',)),
    ('film_actor', ('actor_id', 'film_id',)),
    ('film_category', ('film_id', 'category_id',)),
    ('inventory', ('inventory_id',)),
    ('language', ('language_id',)),
    ('payment', ('payment_id',)),
    ('rental', ('rental_id',)),
    ('staff', ('staff_id',)),
    ('store', ('store_id',)),
}

LIMIT_BY = 250
BASIC_DELIMITER = '-'

"""
    End of block of constants.
"""


if __name__ == '__main__':
    # Connect to google DB
    cred = credentials.Certificate("./serviceAccountKey.json")
    firebase_admin.initialize_app(cred)

    db = firestore.client()

    # Connect to PostgreSQL
    conn = psycopg2.connect(dbname='dvdrental',
                            user='postgres',
                            password='Password',
                            host='localhost',
                            port='5432')
    cur = conn.cursor()

    # Validate all PKs
    for table, pk_list in ORIGINAL_TABLES_WITH_PK:
        cur.execute("SELECT * FROM %s LIMIT 0;" % table)
        colnames = set([desc[0] for desc in cur.description])
        for pk in pk_list:
            if pk not in colnames:
                raise NameError(f'Inside `{table}` there is no column with specified PK - {pk}')

    # Insert data in firebase
    for table, pk_list in ORIGINAL_TABLES_WITH_PK:
        print(f'Transferring data from the table `{table}`')
        offset = 0

        cur.execute("SELECT COUNT(*) FROM %(tablename)s", {'tablename': AsIs(table)})
        num_of_iterations = cur.fetchall()[0][0]

        for iternum in range(ceil(num_of_iterations / LIMIT_BY)):
            print(f'Iteration - {iternum}')

            # Create a batch to collect the queries
            batch = db.batch()
            # Get rows from PostgreSQL
            cur.execute("SELECT * FROM %(tablename)s LIMIT %(limit)s OFFSET %(offset)s ;",
                        {'tablename': AsIs(table), 'limit': LIMIT_BY, 'offset': offset})
            coldesc = [desc for desc in cur.description]
            print(coldesc)
            data = cur.fetchall()

            # Remove PK
            pk_indexes = list()

            for pk in pk_list:
                for i in range(len(coldesc)):
                    if coldesc[i][0] == pk:
                        pk_indexes.append(i)

            # Insert all rows
            for row in data:
                data = dict()
                for i in range(len(row)):
                    if i in pk_indexes or not row[i]:
                        continue

                    if coldesc[i][1] == 17:
                        # Binary objects
                        coldata = base64.b64encode(row[i])
                    elif coldesc[i][1] == 1082:
                        # DateTime objects
                        timestamp = calendar.timegm(row[i].timetuple())
                        coldata = datetime.utcfromtimestamp(timestamp)
                    elif coldesc[i][1] == 1700:
                        # Numeric objects
                        coldata = float(row[i])
                    else:
                        # Other types
                        coldata = row[i]

                    data[coldesc[i][0]] = coldata

                pks = [str(row[pk_idx]) for pk_idx in pk_indexes]
                ref = db.collection(table).document(BASIC_DELIMITER.join(pks))
                batch.set(ref, data)

            # Commit the batch with queries(send rows to FireBase Storage)
            batch.commit()

            offset += LIMIT_BY

        print(f'Finished transferring data from the table `{table}`')

    # Close connection with PostgreSQL DB
    cur.close()
    conn.close()
