# IU DB migration 

A script to migrate PostgreSQL tables to Google Cloud Firestore.


## How to run?
### 1. Install all requirements for the project
Simply run in your terminal:
```
pip install -r requirements.txt
```

### 2. Add your file *serviceAccountKey.json* to the repository
### 3. Setup connection to your PostgreSQL database in the file *main.py*
### 4. Setup required tables in the file *main.py* by changing the constant *ORIGINAL_TABLES_WITH_PK*
#### 5. Finally, run the script *main.py*
